﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;
using UnityEngine.UI;

public class WorldObjects : MonoBehaviour
{
    public string objectName;
    public Sprite buildImage;
    public int hitPoints, maxHitPoints;

    protected PlayerController player;
    protected List<string> actions = new List<string> { };
    protected bool currentlySelected = false;

    public GameObject selectionImage;

    public Color redColor;

    [Header("Cost")]

    [SerializeField] public int foodCost;
    [SerializeField] public int woodCost;
    [SerializeField] public int goldCost;
    [SerializeField] public int stoneCost;

    [Header("Building Material")]

    private List<Material> oldMaterials = new List<Material>();

    [Header("Attack")]

    public bool isMelee = true;
    private bool canattack = true;

    public int attackValue = 3;
    public int attackRange = 5;
    public int ratioOfFire = 2;

    public int armorValue = 2;

    public float lineOfSight = 5;
    public Transform lineOfSightMesh;

    public float weaponRange = 10.0f;

    protected WorldObjects target = null;
    protected bool attacking = false;
    protected bool movingIntoPosition = false;

    protected virtual void Awake()
    {

    }

    protected virtual void Start()
    {
        player = FindObjectOfType<PlayerController>();

        if(lineOfSightMesh)
            lineOfSightMesh.localScale = new Vector3(lineOfSight, 1, lineOfSight);

        if(selectionImage)
            selectionImage.SetActive(false);
    }

    protected virtual void Update()
    {
        if (hitPoints < 0)
            return;

        if (currentlySelected)
        {
            UpdateHealthBar(player.HUDReference.healthSlider, true);
            UpdateStats();
        }
            

        if (target && attacking && Vector3.Distance(transform.position, target.transform.position) < attackRange)
        {
            if (isMelee)
            {
                if (canattack)
                {
                    TakeDamage(attackValue);
                    canattack = false;
                    Invoke("ResetAttack", ratioOfFire);
                }

            }
            else
            {
                UseWeapon();
            }
        }

        if (attacking)
        {
            if(target.hitPoints > 0)
            {
                if (Vector3.Distance(transform.position, target.transform.position) > attackRange)
                    AdjustPosition();
            }
        }
    }

    private void ResetAttack()
    {
        canattack = true;
    }

    private void UpdateStats()
    {
        if (attackValue != 0)
            player.HUDReference.atkText.text = attackValue.ToString();
        else
            player.HUDReference.atkText.text = "";

        if (armorValue != 0)
            player.HUDReference.defText.text = armorValue.ToString();
        else
            player.HUDReference.defText.text = "";
    }

    //The ability to tell it whether it has been selected or not.
    public virtual void SetSelection(bool selected)
    {
        currentlySelected = selected;

        if (currentlySelected)
        {
            if (!selectionImage.activeInHierarchy && selectionImage)
            {
                if (selectionImage)
                    selectionImage.SetActive(true);


                if (gameObject.GetComponent<UnitController>())
                    ServiceLocator.selectionList.Add(this.gameObject.GetComponent<UnitController>());
            }          
        }
        else
        {
            if(selectionImage)
                selectionImage.SetActive(false);

            UpdateHealthBar(player.HUDReference.healthSlider, false);

            if (gameObject.GetComponent<UnitController>())
                ClearSelectionList();
        }            
    }

    private void ClearSelectionList()
    {
        for (int i = 0; i < ServiceLocator.selectionList.Count; i++)
        {
            ServiceLocator.selectionList[i].selectionImage.SetActive(false);
        }

        ServiceLocator.selectionList.Clear();
    }

    public List<string> GetActions()
    {
        return actions;
    }

    public virtual void PerformAction(string actionToPerform, int index)
    {
        
    }

    public virtual void MouseClick(GameObject hitObject, Vector3 hitPoint, PlayerController controller)
    {
        if (currentlySelected && hitObject && hitObject.name != "Ground" && Input.GetMouseButtonDown(0))
        {
            WorldObjects worldObject = hitObject.transform.root.GetComponent<WorldObjects>();

            if (worldObject)
            {
                if (!Input.GetKey(KeyCode.LeftControl))
                    ChangeSelection(worldObject, controller);
                else
                    worldObject.SetSelection(true);
            }
                
        }

        if (currentlySelected && hitObject && hitObject.name != "Ground" && Input.GetMouseButtonDown(1) && CanAttack(hitObject))
        {
            WorldObjects worldObject = hitObject.transform.GetComponent<WorldObjects>();

            BeginAttack(worldObject);
        }
        else
        {
            if (attacking)
                attacking = false;
        }
    }

    //Attack Section

    protected virtual void BeginAttack(WorldObjects target)
    {
        this.target = target;

        SetTarget(target);

        if (TargetInRange())
        {
            attacking = true;
            transform.LookAt(this.target.transform);
            PerformAttack();
        }
        else
            AdjustPosition();
    }

    public virtual void SetTarget(WorldObjects target)
    {
        if(this.target == null)
        {
            this.target = target;
            attacking = true;
        }
    }

    private bool TargetInRange()
    {
        Vector3 targetLocation = target.transform.position;
        Vector3 direction = targetLocation - transform.position;

        if (direction.sqrMagnitude < weaponRange * weaponRange)
            return true;

        return false;
    }

    private void PerformAttack()
    {
        if (!target)
        {
            attacking = false;
            return;
        }

        if (!TargetInRange())
            AdjustPosition();
    }

    private void AdjustPosition()
    {
        UnitController self = this as UnitController;

        if (self)
        {
            movingIntoPosition = true;
            self.SetStartDestination(target.transform.position , attackRange);
            attacking = true;
        }
        else
            attacking = false;
    }

    public virtual void TakeDamage(int damage)
    {
        target.hitPoints -= damage;
        target.SetTarget(this);

        UpdateHealthBar(player.HUDReference.healthSlider, true);

        if (target.hitPoints < 0)
        {
            target.gameObject.layer = 0;
            attacking = false;
            target.AfterDeath();
        }
    }

    public virtual void AfterDeath()
    {

    }

    protected virtual void UseWeapon()
    {
        
    }

    public virtual bool CanAttack(GameObject enemy)
    {
        if (enemy.gameObject.layer == 9)
            return true;
        else
            return false;
    }

    //Selection Section

    private void ChangeSelection(WorldObjects worldObject, PlayerController controller)
    {
        SetSelection(false);
        if (controller.SelectedObject) 
            controller.SelectedObject.SetSelection(false);
        controller.SelectedObject = worldObject;
        worldObject.SetSelection(true);
    }

    public virtual void SetHoverState(GameObject hoverObject)
    {
        if (currentlySelected)
        {
            if (hoverObject.name != "Ground")
            {
               if(hoverObject.gameObject.layer == 9)
                    player.HUDReference.SetCursorState(CursorState.Attack);
               else
                    player.HUDReference.SetCursorState(CursorState.Select);            
            }
        }
    }

    //Health Section

    protected virtual void UpdateHealthBar(Slider slider, bool value)
    {
        slider.gameObject.SetActive(value);

        if(value)
            CalculateCurrentHealth(slider);
    }

    protected virtual void CalculateCurrentHealth(Slider health)
    {
        health.value = (float)hitPoints / (float)maxHitPoints;
    }

    public float GetCurrentHealth()
    {
        return (float)hitPoints / (float)maxHitPoints;
    }

    //Resource Section

    protected void Consume(string name)
    {
        GameObject g = ServiceLocator.GetUnit(name);

        if (!g)
            g = ServiceLocator.GetBuilding(name);

        if (!g)
            g = ServiceLocator.GetUpgrade(name);

        WorldObjects wo = g.GetComponent<WorldObjects>();

        player.AddResource(ResourceType.Food, -wo.foodCost);
        player.AddResource(ResourceType.Wood, -wo.woodCost);
        player.AddResource(ResourceType.Gold, -wo.goldCost);
        player.AddResource(ResourceType.Stone, -wo.stoneCost);
    }

    //Build Section

    public void SetColliders(bool enabled)
    {
        Collider[] colliders = GetComponentsInChildren<Collider>();
        foreach (Collider collider in colliders) collider.enabled = enabled;
    }

    public void SetTransparentMaterial(Material material, bool storeExistingMaterial)
    {
        if (storeExistingMaterial) 
            oldMaterials.Clear();

        Renderer[] renderers = GetComponentsInChildren<MeshRenderer>();

        foreach (Renderer renderer in renderers)
        {
            if (storeExistingMaterial) oldMaterials.Add(renderer.material);
            renderer.material = material;
        }
    }

    public virtual void SetNavMeshObstacle(bool value)
    {

    }

    public void RestoreMaterials()
    {
        Renderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        if (oldMaterials.Count == renderers.Length)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material = oldMaterials[i];
            }
        }
    }

    public virtual Bounds GetSelectionBounds()
    {
        Bounds selectionBounds = new Bounds(transform.position, Vector3.zero);
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
        {
            selectionBounds.Encapsulate(r.bounds);
        }

        return selectionBounds;
    }
}
