﻿using UnityEngine;
using RTS;
using UnityEngine.UI;

public class ResourceController : WorldObjects
{
    public float capacity;

    protected float amountLeft;

    public ResourceType resourceType;

    protected override void Start()
    {
        base.Start();
        amountLeft = capacity;
        resourceType = ResourceType.Unknown;
    }

    public void Remove(float amount)
    {
        amountLeft -= amount;
        if (amountLeft < 0)
            amountLeft = 0;
    }

    public virtual bool isEmpty()
    {
        return amountLeft <= 0;
    }

    public ResourceType GetResourceType()
    {
        return resourceType;
    }

    protected override void CalculateCurrentHealth(Slider slider)
    {
        slider.value = amountLeft / capacity;
    }
}
