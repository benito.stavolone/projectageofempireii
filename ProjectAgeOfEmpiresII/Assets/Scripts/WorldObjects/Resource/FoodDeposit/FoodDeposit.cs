﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RTS;

public class FoodDeposit : ResourceController
{
    protected override void Start()
    {
        base.Start();
        resourceType = ResourceType.Food;

        StartCoroutine(CheckHealth());
    }

    private IEnumerator CheckHealth()
    {
        while(amountLeft > 0)
        {
            yield return null;
        }

        Destroy(gameObject);

    }

    public override void AfterDeath()
    {
        base.AfterDeath();

        Destroy(gameObject);
    }
}
