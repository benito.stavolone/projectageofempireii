﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;

public class WoodDeposit : ResourceController
{
    protected override void Start()
    {
        base.Start();

        resourceType = ResourceType.Wood;

        StartCoroutine(CheckHealth());
    }

    private IEnumerator CheckHealth()
    {
        while (amountLeft > 0)
        {
            yield return null;
        }

        Destroy(gameObject);

    }
}
