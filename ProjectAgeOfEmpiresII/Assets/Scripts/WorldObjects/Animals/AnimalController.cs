﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalController : UnitController
{
    [Header("AnimalType")]

    public AnimalType currentType = AnimalType.None;
    public enum AnimalType
    {
        None,
        Escape,
        Conquest,
        Aggressive
    }

    private FoodDeposit foodDeposit;
    private NavMeshAgent myAgent;

    private int radius = 3;

    protected override void Start()
    {
        base.Start();

        foodDeposit = GetComponent<FoodDeposit>();
        myAgent = GetComponent<NavMeshAgent>();
        foodDeposit.enabled = false;

        StartCoroutine(AnimalMovement());
    }

    private IEnumerator AnimalMovement()
    {
        while (GetCurrentHealth() > 0)
        {
            Vector3 randomDirection = Random.insideUnitSphere * radius;
            randomDirection += transform.position;
            NavMeshHit hit;
            Vector3 finalPosition = Vector3.zero;
            if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
            {
                finalPosition = hit.position;
                SetStartDestination(finalPosition, 0);
            }

            yield return new WaitForSeconds(5);
        }

        myAgent.enabled = false;

        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x + 180, transform.localEulerAngles.y, transform.localEulerAngles.z);

        foodDeposit.enabled = true;
        this.enabled = false;
        
    }

}
