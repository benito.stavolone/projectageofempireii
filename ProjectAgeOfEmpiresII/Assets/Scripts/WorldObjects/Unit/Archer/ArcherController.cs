﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherController : UnitController
{
    public Transform spawnPoint;
    public Projectile arrow;

    private bool canShot = true;

    protected override void UseWeapon()
    {
        base.UseWeapon();

        if (canShot)
        {
            Projectile p = Instantiate(arrow, spawnPoint.position, spawnPoint.transform.rotation);
            p.SetDirection(spawnPoint.forward);
            p.SetTarget(target);
            p.SetDamage(attackValue);
            p.SetArcher(this);

            canShot = false;
            Invoke("ResetShot", ratioOfFire);

            transform.LookAt(target.transform);
        }
    }

    private void ResetShot()
    {
        canShot = true;
    }
}
