﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 10;

    private int damage = 1;
    private WorldObjects target;
    private WorldObjects archer;

    private Vector3 direction = Vector3.zero;
    private bool shot = false;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyBullet", 5);
    }

    public void SetDirection(Vector3 direction)
    {
        this.direction = direction;
    }

    public void SetTarget(WorldObjects wo)
    {
        target = wo;
    }

    public void SetArcher(WorldObjects archer)
    {
        this.archer = archer;
    }

    public void SetDamage(int damage)
    {
        this.damage = damage;
    }

    public void SetShotBool(bool value)
    {
        this.shot = value;
    }

    // Update is called once per frame
    void Update()
    {
        if (HitSomething())
        {
            InflictDamage();
            Destroy(gameObject);
        }

        if(shot)
            transform.position += direction * speed * Time.deltaTime;

    }

    private bool HitSomething()
    {
        if (target && target.GetSelectionBounds().Contains(transform.position)) 
            return true;
        return false;
    }

    private void InflictDamage()
    {
        if (target.GetCurrentHealth() > 0)
            archer.TakeDamage(damage);
        else
            archer.SetTarget(null);
    }

    private void DestroyBullet()
    {
        if (gameObject)
            Destroy(gameObject);
    }
}
