﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;
using System;

public class VillagerController : UnitController
{
    public float capacity;
    public BuildingController resourceStore;
    public BuildingController enemyResourceStore;

    private BuildingController standardDeposit;

    private bool harvesting = false, emptying = false;
    private float currentLoad = 0.0f;
    private ResourceType harvestType;
    private ResourceController resourceDeposit;

    public float collectionAmount, depositAmount;

    private float currentDeposit = 0.0f;


    private BuildingController currentProject;
    private bool building = false;
    private float amountBuilt = 0.0f;

    private bool isRinging = false;
    private Transform emergencyBuilder;

    protected override void Start()
    {
        base.Start();
        harvestType = ResourceType.Unknown;

        EventManager.storageBuilt += CheckNearestStorage;

        actions = new List<string>() { "House", "Barracks", "Granary", "StorageWood", "StorageGold", "StorageStone", "Armory" };

        if(gameObject.layer == 9)
        {
            GiveJob(enemyResourceStore);
        }
    }

    private void OnEnable()
    {
        EventManager.newAge += AddActions;
        EventManager.dinDon += OnBellsRing;
    }

    protected override void Update()
    {
        base.Update();

        if (building && currentProject && currentProject.UnderConstruction())
        {
            amountBuilt += 2 * Time.deltaTime;
            int amount = Mathf.FloorToInt(amountBuilt);
            if (amount > 0)
            {
                amountBuilt -= amount;
                currentProject.Construct(amount);
                if (!currentProject.UnderConstruction()) building = false;
            }
        }

        if (harvesting || emptying)
        {
            if (harvesting)
            {
                if (resourceDeposit == null)
                {
                    CheckDeposit();
                    CheckNearestDeposit();
                    return;
                }


                if (Vector3.Distance(transform.position, resourceDeposit.transform.position) <= 15 && resourceDeposit)
                    Collect();

                if (currentLoad >= capacity || resourceDeposit.isEmpty())
                {
                    currentLoad = Mathf.Floor(currentLoad);

                    harvesting = false;
                    emptying = true;

                    if (resourceStore == null)
                    {
                        CheckStorage();
                    }
                    else
                    {
                        SetStartDestination(resourceStore.transform.position, 1);
                    }
                }
            }
            else
            {
                if (gameObject.layer == 9)
                    resourceStore = enemyResourceStore;

                if (resourceStore == null)
                {
                    CheckStorage();

                    if (gameObject.layer != 9 && resourceStore == null)
                        resourceStore = FindTownCenter();
                }
                else
                {
                    if (Vector3.Distance(transform.position, resourceStore.transform.position) <= 10)
                        Deposit();
                }

                if (currentLoad <= 0)
                {
                    emptying = false;

                    if (!resourceDeposit.isEmpty())
                    {
                        harvesting = true;
                        SetStartDestination(resourceDeposit.transform.position, 1);
                    }
                    else
                    {
                        CheckDeposit();
                        //CheckNearestDeposit();
                    }
                }
            }
        }

        if (isRinging)
        {
            if(Vector3.Distance(transform.position, emergencyBuilder.position) < 7)
            {
                gameObject.transform.localScale = Vector3.zero;
                SetStartDestination(transform.position, 0);
            }
        }

        if(!harvesting && !emptying && target && gameObject.layer == 9)
        {
            if(target.hitPoints < 0)
            {
                target = null;
                attacking = false;
                GiveJob(enemyResourceStore);
            }
        }

        if(resourceStore == null)
        {
            Debug.Log(harvestType);
            resourceStore = FindTownCenter();
            CheckStorage();
        }

    }

    private BuildingController FindTownCenter()
    {
        TownCenterController townCenter = FindObjectOfType<TownCenterController>();

        if (townCenter.gameObject.layer != 9)
            return townCenter.gameObject.GetComponent<BuildingController>();
        else
            return null;
    }

    private void OnDisable()
    {
        EventManager.newAge -= AddActions;
        EventManager.dinDon -= OnBellsRing;
    }

    //Resourse Section-------

    public override void SetHoverState(GameObject hoverObject)
    {
        base.SetHoverState(hoverObject);

        if (player && player.human && currentlySelected)
        {
            if (hoverObject.name != "Ground")
            {
                ResourceController resource = hoverObject.transform.GetComponent<ResourceController>();

                if (resource && !resource.isEmpty()) 
                    player.HUDReference.SetCursorState(CursorState.Resources);
            }
        }
    }

    public override void MouseClick(GameObject hitObject, Vector3 hitPoint, PlayerController controller)
    {
        base.MouseClick(hitObject, hitPoint, controller);

        if (player && player.human)
        {
            if (hitObject.name != "Ground")
            {
                ResourceController resource = hitObject.GetComponentInParent<ResourceController>();

                if (resource && !resource.isEmpty())
                {
                    StartHarvest(resource);
                }
                else
                {
                    try
                    {
                        BuildingController buildings = hitObject.transform.parent.GetComponent<BuildingController>();

                        if (buildings)
                        {
                            if (buildings)
                            {
                                if (buildings.UnderConstruction())
                                {
                                    SetStartDestination(buildings.transform.position, 7);
                                    building = true;
                                }
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        BuildingController buildings = hitObject.transform.GetComponent<BuildingController>();

                        if (buildings)
                        {
                            if (buildings.UnderConstruction())
                            {
                                SetStartDestination(buildings.transform.position, 7);
                                buildings.ClearBuilderList();
                                buildings.SetBuilder();
                                building = true;
                            }
                        }
                    }
                }             
            }
            else
            {
                StopHarvest();
                building = false;
            }
                        
        }

    }

    public void StartHarvest(ResourceController resource)
    {
        resourceDeposit = resource;
        SetStartDestination(resource.transform.position, resource.GetSelectionBounds().size.z + 2);

        if (harvestType == ResourceType.Unknown || harvestType != resource.GetResourceType())
        {
            harvestType = resource.GetResourceType();
            currentLoad = 0.0f;
            CheckStorage();
        }

        harvesting = true;
        emptying = false;
    }

    private void StopHarvest()
    {

    }

    private void Collect()
    {
        float collect = collectionAmount * Time.deltaTime;

        if (currentLoad + collect > capacity) 
            collect = capacity - currentLoad;

        resourceDeposit.Remove(collect);
        currentLoad += collect;
    }

    private void Deposit()
    {
        currentDeposit += depositAmount * Time.deltaTime;
        int deposit = Mathf.FloorToInt(currentDeposit);

        if (deposit >= 1)
        {
            if (deposit > currentLoad) 
                deposit = Mathf.FloorToInt(currentLoad);
            currentDeposit -= deposit;
            currentLoad -= deposit;
            ResourceType depositType = harvestType;
            if (harvestType == ResourceType.Gold) 
                depositType = ResourceType.Gold;

            if(gameObject.layer != 9)
                player.AddResource(depositType, deposit);
        }
    }

    private void CheckStorage()
    {
        Collider[] objectInscene = Physics.OverlapSphere(transform.localPosition, 40);

        for (int i = 0; i < objectInscene.Length; i++)
        {
            if (objectInscene[i].GetComponent<StorageController>() != null)
            {
                if (objectInscene[i].GetComponent<StorageController>().Resource == harvestType)
                {
                    resourceStore = objectInscene[i].GetComponent<BuildingController>();
                    break;
                }             
                else
                    resourceStore = FindTownCenter();
            }
        }
    }

    private void CheckNearestStorage(ResourceType resourceType, BuildingController storagePosition)
    {
        if(harvestType == resourceType)
        {
            if (!storagePosition.UnderConstruction())
            {
                if (Vector3.Distance(resourceDeposit.transform.position, resourceStore.transform.position)
                        > Vector3.Distance(resourceDeposit.transform.position, storagePosition.transform.position))
                {
                    resourceStore = storagePosition;
                }
            }
        }
    }

    private void CheckNearestDeposit()
    {
        int distance = 0;

        if (gameObject.layer == 9)
            distance = 100;
        else
            distance = 20;

        Collider[] objectInscene = Physics.OverlapSphere(transform.localPosition, distance);

        for (int i = 0; i < objectInscene.Length; i++)
        {
            if (objectInscene[i].GetComponent<ResourceController>() != null)
            {
                resourceDeposit = objectInscene[i].GetComponent<ResourceController>();
                harvestType = objectInscene[i].GetComponent<ResourceController>().GetResourceType();
            }
        }
    }

    //Building Section ---------

    public override void SetBuilding(BuildingController creator)
    {
        base.SetBuilding(creator);

        currentProject = creator;

        for(int i = 0; i < ServiceLocator.selectionList.Count; i++)
        {
            ServiceLocator.selectionList[i].SetStartDestination(currentProject.transform.position, currentProject.GetSelectionBounds().size.z + 2);
            currentProject.SetBuilder();
        }

        harvesting = false;
        emptying = false;
        building = true;
    }

    public override void PerformAction(string actionToPerform, int index)
    {
        base.PerformAction(actionToPerform, index);
        CreateBuilding(actionToPerform);
    }

    private void CreateBuilding(string buildingName)
    {
        WorldObjects wo = ServiceLocator.GetBuilding(buildingName).GetComponent<WorldObjects>();

        if (player.GetResource(ResourceType.Food) >= wo.foodCost
            && player.GetResource(ResourceType.Wood) >= wo.woodCost
            && player.GetResource(ResourceType.Gold) >= wo.goldCost
            && player.GetResource(ResourceType.Stone) >= wo.stoneCost)
        {
            Vector3 buildPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z + 10);
            if (player)
                player.CreateBuilding(buildingName, buildPoint, this);
        }
    }

    public override void SetStartDestination(Vector3 destination, float stoppingDistance)
    {
        base.SetStartDestination(destination, stoppingDistance);
        amountBuilt = 0.0f;
    }

    private void CheckDeposit()
    {
        int distance = 0;

        if (gameObject.layer == 9)
            distance = 100;
        else
            distance = 40;

        Collider[] objectInscene = Physics.OverlapSphere(transform.localPosition, distance);

        for (int i = 0; i < objectInscene.Length; i++)
        {
            if (objectInscene[i].GetComponent<ResourceController>() != null)
            {
                if (objectInscene[i].GetComponent<ResourceController>().GetResourceType() == harvestType)
                {
                    StartHarvest(objectInscene[i].GetComponent<ResourceController>());
                    break;
                }
            }
        }
    }

    public override void SetTarget(WorldObjects target)
    {
        base.SetTarget(target);

        harvesting = false;
    }

    private void AddActions()
    {
        actions.Add("Turret");
    }

    private void OnBellsRing(bool state)
    {
        if(gameObject.layer != 9)
        {
            isRinging = state;

            if (isRinging)
            {
                Collider[] objectInscene = Physics.OverlapSphere(transform.localPosition, 100);

                for (int i = 0; i < objectInscene.Length; i++)
                {
                    if (objectInscene[i].GetComponent<IEmergencyBuilder>() != null)
                    {
                        emergencyBuilder = objectInscene[i].transform;
                        SetStartDestination(objectInscene[i].transform.position, 0);
                    }
                }
            }
            else
            {
                SetStartDestination(transform.position, 0);

                transform.localScale = Vector3.one;
            }
        }
    }

    public override void GiveJob(BuildingController Store)
    {
        base.GiveJob(resourceStore);

        enemyResourceStore = Store;
        resourceStore = enemyResourceStore;
        CheckNearestDeposit();
        CheckDeposit();
        CheckDeposit();
    }
}
