﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using RTS;
using UnityEngine.UI;

public class UnitController : WorldObjects
{
    //Movement Settings

    public float moveSpeed, rotateSpeed;

    public int populationCost;

    protected Vector3 destination;
    protected NavMeshAgent pathfinding;

    protected override void Awake()
    {
        base.Awake();

        pathfinding = gameObject.GetComponent<NavMeshAgent>();
    }

    protected override void Start()
    {
        base.Start();

        if(gameObject.layer != 9)
            player.AddResource(ResourceType.Population, populationCost);

        if(GetComponent<AnimalController>() == null)
            StartCoroutine(CheckHealth());

        searchingTarget = StartCoroutine(FindEnemy());
    }

    private IEnumerator CheckHealth()
    {
        while (hitPoints > 0)
        {
            yield return null;
        }

        gameObject.SetActive(false);
    }

    public override void SetHoverState(GameObject hoverObject)
    {
        base.SetHoverState(hoverObject);

        if (player && currentlySelected)
        {
            if (hoverObject.name == "Ground")
            {
                player.HUDReference.SetCursorState(CursorState.Move);
            }
        }
    }

    public override void MouseClick(GameObject hitObject, Vector3 hitPoint, PlayerController controller)
    {
        base.MouseClick(hitObject, hitPoint, controller);

        if (player && currentlySelected)
        {       
            if (hitObject.name == "Ground" && hitPoint != ServiceLocator.InvalidPosition && Input.GetMouseButtonDown(1))
            {               
                float x = hitPoint.x;
                float y = hitPoint.y + player.SelectedObject.transform.position.y;
                float z = hitPoint.z;
                Vector3 destination = new Vector3(x, y, z);

                SetStartDestination(destination, 3);
            }
        }
    }

    public virtual void SetStartDestination(Vector3 destination, float stoppingDistance)
    {
        if (gameObject.activeInHierarchy)
        {
            pathfinding.stoppingDistance = stoppingDistance;
            pathfinding.SetDestination(destination);
        }
    }

    public virtual void SetBuilding(BuildingController creator)
    {
        
    }

    public void SetLayer(LayerMask layerMask)
    {
        gameObject.layer = 9;

        gameObject.GetComponent<MeshRenderer>().material.color = Color.red;

        selectionImage.GetComponent<Image>().color = redColor;

        lineOfSightMesh.gameObject.SetActive(false);

        layer = layerMask;
    }

    public virtual void GiveJob(BuildingController resourceStore)
    {

    }

    //Attack Section

    private Coroutine searchingTarget = null;
    public LayerMask layer;

    private IEnumerator FindEnemy()
    {
        float refreshRate = 1.5f;

        while (target == null)
        {
            Collider[] enemyInScene = Physics.OverlapSphere(transform.localPosition, 20, layer);

            for (int i = 0; i < enemyInScene.Length; i++)
            {
                if(enemyInScene[i].GetComponent<UnitController>() || enemyInScene[i].GetComponent<BuildingController>())
                {
                    target = enemyInScene[i].GetComponent<WorldObjects>();
                    break;
                }
            }

            yield return new WaitForSeconds(refreshRate);
        }
    }

    protected override void Update()
    {
        base.Update();

        if(gameObject.layer == 9)
        {
            if (target)
            {
                searchingTarget = null;

                if (searchingTarget == null)
                    BeginAttack(target);
            }
            else
            {
                if (searchingTarget == null)
                    searchingTarget = StartCoroutine(FindEnemy());
            }
        }   
    }
}
