﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmoryController : BuildingController
{
    [Header("Upgrade")]

    public List<TechNode> node = new List<TechNode>();

    protected override void Start()
    {
        base.Start();
        actions = new List<string>() { "IncreaseArmor", "IncreaseAttack"};
    }

    public override void PerformAction(string actionToPerform, int index)
    {
        base.PerformAction(actionToPerform, index);

        UpgradeProgression(node[index]);

        player.HUDReference.buttonReference[index].gameObject.SetActive(false);

        actions.Remove(actionToPerform);
    }

    public override void AfterConstruction()
    {
        base.AfterConstruction();

        //take reference
        panelCreation = player.HUDReference.PanelCreation;

        EventManager.ArmoryBuilt();
    }
}
