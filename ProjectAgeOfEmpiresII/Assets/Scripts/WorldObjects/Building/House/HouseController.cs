﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;

public class HouseController : BuildingController
{
    [SerializeField] private int incrementPopulation = 5;

    protected override void Start()
    {
        base.Start();
    }

    public override void AfterConstruction()
    {
        base.AfterConstruction();

        player.IncrementResourceLimit(ResourceType.Population, incrementPopulation);
    }

    protected override void Destroying()
    {
        base.Destroying();

        player.IncrementResourceLimit(ResourceType.Population, -incrementPopulation);
    }
}
