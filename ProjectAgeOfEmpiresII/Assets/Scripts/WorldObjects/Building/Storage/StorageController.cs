﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;

public class StorageController : BuildingController
{
    [SerializeField] private ResourceType resourceType;

    public ResourceType Resource
    {
        get { return resourceType; }
    }

    public override void AfterConstruction()
    {
        base.AfterConstruction();

        EventManager.StorageBuilt(resourceType, this);

        BuilderDecisions();
    }

    private void BuilderDecisions()
    {
        CheckDeposit();
        ClearBuilderList();
    }

    private void CheckDeposit()
    {
        Collider[] objectInscene = Physics.OverlapSphere(transform.localPosition, 10);

        for (int i = 0; i < objectInscene.Length; i++)
        {
            if (objectInscene[i].GetComponent<ResourceController>() != null)
            {
                if (objectInscene[i].GetComponent<ResourceController>().GetResourceType() == resourceType)
                {
                    for (int j = 0; j < builderList.Count; j++)
                    {
                        builderList[j].GetComponent<VillagerController>().StartHarvest(objectInscene[i].GetComponent<ResourceController>());
                    }

                    break;
                }
            }
        }
    }
}
