﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarracksController : BuildingController
{
    protected override void Start()
    {
        base.Start();
        actions = new List<string>() { "Militia" , "Archer"};
    }

    public override void PerformAction(string actionToPerform, int index)
    {
        base.PerformAction(actionToPerform, index);
        CreateUnit(actionToPerform);      
    }

    public override void AfterConstruction()
    {
        base.AfterConstruction();

        //take reference
        panelCreation = player.HUDReference.PanelCreation;
    }
}
