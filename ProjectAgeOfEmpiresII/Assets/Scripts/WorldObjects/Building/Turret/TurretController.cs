﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : BuildingController, IEmergencyBuilder
{
    public Transform spawnPointArrow;
    public Projectile arrow;

    public LayerMask layer;
    private WorldObjects targetTurret;
    private bool canShot = true;


    private Coroutine searchingTarget = null;

    protected override void Start()
    {
        base.Start();

        searchingTarget = StartCoroutine(FindEnemy());
    }

    private IEnumerator FindEnemy()
    {
        float refreshRate = 1.5f;

        while(targetTurret == null || target == null)
        {
            Collider[] enemyInScene = Physics.OverlapSphere(transform.localPosition, 20, layer);

            for (int i = 0; i < enemyInScene.Length; i++)
            {
                if (enemyInScene[i].GetComponent<AnimalController>())
                    continue;
                else
                {
                    targetTurret = enemyInScene[i].GetComponent<WorldObjects>();
                    break;
                }
            }         

            yield return new WaitForSeconds(refreshRate);
        }
    }

    protected override void Update()
    {
        base.Update();

        if (targetTurret)
        {
            target = targetTurret;
            searchingTarget = null;
            if (canShot)
            {
                spawnPointArrow.LookAt(targetTurret.transform);

                Projectile p = Instantiate(arrow, spawnPointArrow.position, spawnPointArrow.transform.rotation);
                p.SetDirection(spawnPointArrow.forward);
                p.SetTarget(targetTurret);
                p.SetDamage(attackValue);
                p.SetArcher(this);
                p.SetShotBool(true);

                canShot = false;
                Invoke("ResetShot", ratioOfFire);
            }
        }
        else
        {
            if (searchingTarget == null)
                searchingTarget = StartCoroutine(FindEnemy());
        }
    }

    public override void SetTarget(WorldObjects target)
    {
        targetTurret = target;
    }

    private void ResetShot()
    {
        canShot = true;
    }

    public void ChangeLayer(int layernumber)
    {
        layer.value = layernumber;
    }
}
