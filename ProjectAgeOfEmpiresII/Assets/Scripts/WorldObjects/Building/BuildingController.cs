﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;
using UnityEngine.UI;
using UnityEngine.AI;

public class BuildingController : WorldObjects
{
    [Header("Building Settings")]

    [SerializeField] private float maxBuildProgress;
    [SerializeField] private Transform spawnPoint;

    protected Queue<string> buildQueue;
    protected Queue<TechNode> upgradeQueue;

    private float currentBuildProgress = 0.0f;
    private float currentUpgradeProgress = 0f;
    private string currentUnit;

    private bool needsBuilding = false;
    private NavMeshObstacle navMeshObstacle;

    protected List<UnitController> builderList = new List<UnitController>();

    [Header("UI Reference")]

    [SerializeField] protected GameObject panelCreation;
    [SerializeField] private GameObject buttonToIstantiate;

    private List<GameObject> myButtons;

    //Rally Point Section

    protected Vector3 rallyPoint;



    protected override void Awake()
    {
        base.Awake();

        buildQueue = new Queue<string>();
        upgradeQueue = new Queue<TechNode>();

        myButtons = new List<GameObject>();

        if(spawnPoint)
            rallyPoint = spawnPoint.position;
    }

    protected override void Start()
    {
        base.Start();

        navMeshObstacle = GetComponent<NavMeshObstacle>();
        navMeshObstacle.enabled = true;

        StartCoroutine(CheckHealth());
    }

    protected override void Update()
    {
        base.Update();

        ProcessBuildQueue();

        ProcessUpgradeQueue();

        ShowHideQueue();
    }

    protected void CreateUnit(string unitName)
    {
        if(buildQueue.Count <= 9)
        {
            currentUnit = unitName;
            WorldObjects wo = ServiceLocator.GetUnit(unitName).GetComponent<WorldObjects>();

            if (player.GetResource(ResourceType.Food) >= wo.foodCost
                && player.GetResource(ResourceType.Wood) >= wo.woodCost
                && player.GetResource(ResourceType.Gold) >= wo.goldCost
                && player.GetResource(ResourceType.Stone) >= wo.stoneCost)
            {
                buildQueue.Enqueue(unitName);
                Consume(unitName);


                GameObject button = Instantiate(buttonToIstantiate, panelCreation.transform);
                button.GetComponent<Button>().image.sprite = ServiceLocator.GetBuildImage(unitName);
                myButtons.Add(button);

            }
        }
    }

    protected bool UpgradeProgression(TechNode currentUpgrade)
    {
        TechNode node = ServiceLocator.GetUpgrade(currentUpgrade.name).GetComponent<TechNode>();

        if (player.GetResource(ResourceType.Food) >= node.foodCost
            && player.GetResource(ResourceType.Wood) >= node.woodCost
            && player.GetResource(ResourceType.Gold) >= node.goldCost
            && player.GetResource(ResourceType.Stone) >= node.stoneCost)
        {
            upgradeQueue.Enqueue(currentUpgrade);
            Consume(currentUpgrade.name);


            GameObject button = Instantiate(buttonToIstantiate, panelCreation.transform);
            button.GetComponent<Button>().image.sprite = currentUpgrade.buildImage;
            myButtons.Add(button);

            return true;
        }
        else
            return false;
    }

    protected void ProcessBuildQueue()
    {
        if (buildQueue.Count > 0)
        {
            if (player.GetResource(ResourceType.Population) + ServiceLocator.GetUnit(currentUnit).GetComponent<UnitController>().populationCost <= player.GetPopulationLimits())
            {
                currentBuildProgress += Time.deltaTime * ServiceLocator.BuildSpeed;
                myButtons[0].GetComponent<Button>().image.fillAmount = 1 - ((1 * currentBuildProgress) / maxBuildProgress);

                if (currentBuildProgress > maxBuildProgress)
                {
                    if (player)
                    {
                        player.AddUnit(buildQueue.Dequeue(), spawnPoint.position, rallyPoint, transform.rotation, this);
                        Destroy(myButtons[0]);
                        myButtons.RemoveAt(0);
                    }
                    currentBuildProgress = 0.0f;
                }
            }

        }
    }

    protected void ProcessUpgradeQueue()
    {
        if (upgradeQueue.Count > 0)
        {
            currentUpgradeProgress += Time.deltaTime * ServiceLocator.BuildSpeed;
            myButtons[0].GetComponent<Button>().image.fillAmount = 1 - ((1 * currentUpgradeProgress) / maxBuildProgress);

            if (currentUpgradeProgress > maxBuildProgress)
            {
                if (player)
                {
                    upgradeQueue.Dequeue().ApplyEffect();
                    Destroy(myButtons[0]);
                    myButtons.RemoveAt(0);
                }

                currentUpgradeProgress = 0.0f;
            }
        }
    }

    private void ShowHideQueue()
    {
        if (currentlySelected && (buildQueue.Count > 0 || upgradeQueue.Count > 0))
        {
            for (int i = 0; i < myButtons.Count; i++)
                myButtons[i].SetActive(true);
        }
        else
        {
            for (int i = 0; i < myButtons.Count; i++)
                myButtons[i].SetActive(false);
        }
    }

    public string[] getBuildQueueValues()
    {
        string[] values = new string[buildQueue.Count];
        int pos = 0;
        foreach (string unit in buildQueue) values[pos++] = unit;
        return values;
    }

    public float getBuildPercentage()
    {
        return maxBuildProgress;
    }

    public void SetBuilder()
    {
        for(int i = 0; i < ServiceLocator.selectionList.Count; i++)
        {
            builderList.Add(ServiceLocator.selectionList[i]);
        }
    }

    public void ClearBuilderList()
    {
        builderList.Clear();
    }

    //RallyPoint Section

    public override void SetSelection(bool selected)
    {
        base.SetSelection(selected);

        if (player && spawnPoint && gameObject.layer != 9)
        {
            RallyPoint flag = player.RallyPoint;

            if (selected)
            {
                if (flag && player.human && spawnPoint.position != ServiceLocator.InvalidPosition && rallyPoint != ServiceLocator.InvalidPosition)
                {

                    flag.transform.localPosition = rallyPoint;
                    flag.transform.forward = transform.forward;
                    flag.Enable();
                }
            }
            else
            {
                if (flag && player.human) 
                    flag.Disable();
            }
        }
    }

    public override void MouseClick(GameObject hitObject, Vector3 hitPoint, PlayerController controller)
    {
        base.MouseClick(hitObject, hitPoint, controller);

        if (player && currentlySelected && spawnPoint)
        {
            if (hitObject.name == "Ground")
            {
                if (hitPoint != ServiceLocator.InvalidPosition)
                {
                    SetRallyPoint(hitPoint);
                }
            }
        }
    }

    public void SetRallyPoint(Vector3 position)
    {
        rallyPoint = position;

        if (player && player.human && currentlySelected)
        {
            RallyPoint flag = player.RallyPoint;

            if (flag) 
                flag.transform.localPosition = rallyPoint;
        }
    }

    public void StartConstruction()
    {
        SetNavMeshObstacle(true);
        needsBuilding = true;
        hitPoints = 0;
    }

    public bool UnderConstruction()
    {
        return needsBuilding;
    }

    public void Construct(int amount)
    {
        hitPoints += amount;
        if (hitPoints >= maxHitPoints)
        {
            hitPoints = maxHitPoints;
            needsBuilding = false;
            RestoreMaterials();
            AfterConstruction();
        }
    }

    public override Bounds GetSelectionBounds()
    {
        return base.GetSelectionBounds();
    }

    public virtual void AfterConstruction()
    {
        Consume(objectName);
        player.HUDReference.ActiveActionPanel();
    }

    public override void SetNavMeshObstacle(bool value)
    {
        base.SetNavMeshObstacle(value);

        navMeshObstacle.enabled = value;
    }

    public void SetLayer()
    {
        gameObject.layer = 9;

        selectionImage.GetComponent<Image>().color = redColor;

        lineOfSightMesh.gameObject.SetActive(false);
    }

    private IEnumerator CheckHealth()
    {
        while (hitPoints >= 0)
        {
            yield return null;
        }

        Destroying();
    }

    protected virtual void Destroying()
    {
        if (!UnderConstruction())
            Destroy(gameObject);
    }
}
