﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownCenterController : BuildingController, IEmergencyBuilder
{
    public List<TechNode> agesList = new List<TechNode>();

    private bool hasArmory = false;

    public Transform spawnPointArrow;
    public Projectile arrow;

    public LayerMask layer;
    private WorldObjects targetTurret;
    private bool canShot = true;

    private bool bellInside = false;
    private Coroutine searchingTarget = null;

    public string MessageWhenDestroyed;


    private void OnEnable()
    {
        EventManager.armoryBuilt += OnArmoryBuilt;
    }

    private void OnDisable()
    {
        EventManager.armoryBuilt -= OnArmoryBuilt;
    }

    protected override void Start()
    {
        base.Start();
        actions = new List<string>() { "Villager" , "FeudalAge" , "Bells" };

        SetNavMeshObstacle(true);

        searchingTarget = StartCoroutine(FindEnemy());
    }

    private IEnumerator FindEnemy()
    {
        float refreshRate = 1.5f;

        while (targetTurret == null)
        {
            Collider[] enemyInScene = Physics.OverlapSphere(transform.localPosition, 20, layer);

            for (int i = 0; i < enemyInScene.Length; i++)
            {
                targetTurret = enemyInScene[i].GetComponent<WorldObjects>();
                break;
            }

            yield return new WaitForSeconds(refreshRate);
        }
    }

    protected override void Update()
    {
        base.Update();

        if (targetTurret)
        {
            target = targetTurret;
            searchingTarget = null;
            if (canShot)
            {

                spawnPointArrow.LookAt(targetTurret.transform);

                Projectile p = Instantiate(arrow, spawnPointArrow.position, spawnPointArrow.transform.rotation);
                p.SetDirection(spawnPointArrow.forward);
                p.SetTarget(targetTurret);
                p.SetDamage(attackValue);
                p.SetArcher(this);
                p.SetShotBool(true);

                canShot = false;
                Invoke("ResetShot", ratioOfFire);

            }
        }
        else
        {
            if (searchingTarget == null)
                searchingTarget = StartCoroutine(FindEnemy());
        }
    }

    public override void SetTarget(WorldObjects target)
    {
        targetTurret = target;
    }

    private void ResetShot()
    {
        canShot = true;
    }

    public override void PerformAction(string actionToPerform, int index)
    {
        base.PerformAction(actionToPerform, index);

        if(actionToPerform == "Villager")
            CreateUnit(actionToPerform);

        if(actionToPerform == "FeudalAge" && hasArmory)
        {
            if (UpgradeProgression(agesList[0]))
            {
                player.HUDReference.buttonReference[index].gameObject.SetActive(false);
                EventManager.NewAge();
                actions.Remove("FeudalAge");
            }
        }

        if(actionToPerform == "Bells")
        {
            if(bellInside == false)
            {
                bellInside = true;
            }
            else
            {
                bellInside = false;
            }

            EventManager.DinDon(bellInside);
        }
    }

    private void OnArmoryBuilt()
    {
        hasArmory = true;
    }

    protected override void Destroying()
    {
        base.Destroying();

        EventManager.GameOver(MessageWhenDestroyed);
    }
}
