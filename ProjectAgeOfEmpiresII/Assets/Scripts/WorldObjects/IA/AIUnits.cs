﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIUnits : MonoBehaviour
{
    private List<UnitController> enemyUnits = new List<UnitController>();
    private int unitsCounter = 0;

    public Transform attackPosition;

    public void AddUnitInList(UnitController unit)
    {
        enemyUnits.Add(unit);
        unitsCounter++;

        if(unitsCounter >= 5)
        {
            TimeToAttack();
        }
    }

    private void TimeToAttack()
    {
        for(int i = 0; i < enemyUnits.Count; i++)
        {
            enemyUnits[i].SetStartDestination(attackPosition.position, 0);
        }

        unitsCounter = 0;
        enemyUnits.Clear();
    }
}
