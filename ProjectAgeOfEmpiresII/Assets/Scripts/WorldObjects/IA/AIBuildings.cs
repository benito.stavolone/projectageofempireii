﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;

public class AIBuildings : MonoBehaviour
{
    public LayerMask myEnenmyLayer;

    [Header("Buildings")]

    public List<BuildingController> buildingList = new List<BuildingController>();

    public BuildingController townCenter;

    public float minTimeRange = 20;
    public float maxTimeRange = 30;

    [Header("Unit")]

    public List<UnitController> unitList = new List<UnitController>();

    public Transform[] unitSpawnPoint;

    public AIUnits AiUnits;

    private void Start()
    {
        Build();
    }

    private void Build()
    {
        float timeRange = Random.Range(minTimeRange, maxTimeRange + 1);

        float percentage = Random.Range(0, 100);

        if (percentage < 50)
            Invoke("Costruct", timeRange);
        else
            Invoke("Create", timeRange);
    }

    private void Costruct()
    {
        int buildingIndex = Random.Range(0, buildingList.Count);

        BuildingController b = null;

        float positionX = Random.Range(0f, 45f);
        float positionZ = Random.Range(0f, 45f);

        if (CanPlaceBuilding(buildingList[buildingIndex]))
        {
            int position = Random.Range(0, 4);

            switch (position)
            {
                case 0:
                    b = Instantiate(buildingList[buildingIndex], new Vector3(townCenter.transform.position.x + positionX, 0.5f, townCenter.transform.position.z + positionZ), Quaternion.identity);
                    break;
                case 1:
                    b = Instantiate(buildingList[buildingIndex], new Vector3(townCenter.transform.position.x + positionX, 0.5f, townCenter.transform.position.z - positionZ), Quaternion.identity);
                    break;
                case 2:
                    b = Instantiate(buildingList[buildingIndex], new Vector3(townCenter.transform.position.x - positionX, 0.5f, townCenter.transform.position.z + positionZ), Quaternion.identity);
                    break;
                case 3:
                    b = Instantiate(buildingList[buildingIndex], new Vector3(townCenter.transform.position.x - positionX, 0.5f, townCenter.transform.position.z - positionZ), Quaternion.identity);
                    break;
            }
            
            b.SetLayer();

            if (b.GetComponent<TurretController>())
                b.GetComponent<TurretController>().ChangeLayer(0);

            Build();
        }
        else
            Costruct();   
    }

    public bool CanPlaceBuilding(BuildingController build)
    {
        bool canPlace = true;

        Bounds placeBounds = build.GetSelectionBounds();
        //shorthand for the coordinates of the center of the selection bounds
        float cx = placeBounds.center.x;
        float cy = placeBounds.center.y;
        float cz = placeBounds.center.z;
        //shorthand for the coordinates of the extents of the selection box
        float ex = placeBounds.extents.x;
        float ey = placeBounds.extents.y;
        float ez = placeBounds.extents.z;

        //Determine the screen coordinates for the corners of the selection bounds
        List<Vector3> corners = new List<Vector3>();

        corners.Add(Camera.main.WorldToScreenPoint(placeBounds.center));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex, cy + ey, cz + ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex, cy + ey, cz - ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex, cy - ey, cz + ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex, cy + ey, cz + ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex, cy - ey, cz - ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex, cy - ey, cz + ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex, cy + ey, cz - ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex, cy - ey, cz - ez)));

        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 2, cy + ey / 2, cz + ez / 2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 2, cy + ey / 2, cz - ez / 2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 2, cy - ey / 2, cz + ez / 2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 2, cy + ey / 2, cz + ez / 2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 2, cy - ey / 2, cz - ez / 2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 2, cy - ey / 2, cz + ez / 2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 2, cy + ey / 2, cz - ez / 2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 2, cy - ey / 2, cz - ez / 2)));

        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 4, cy + ey / 4, cz + ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 4, cy + ey / 4, cz - ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 4, cy - ey / 4, cz + ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 4, cy + ey / 4, cz + ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 4, cy - ey / 4, cz - ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 4, cy - ey / 4, cz + ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 4, cy + ey / 4, cz - ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 4, cy - ey / 4, cz - ez / 4)));

        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 6, cy + ey / 6, cz + ez / 6)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 6, cy + ey / 6, cz - ez / 6)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 6, cy - ey / 6, cz + ez / 6)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 6, cy + ey / 6, cz + ez / 6)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 6, cy - ey / 6, cz - ez / 6)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 6, cy - ey / 6, cz + ez / 6)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 6, cy + ey / 6, cz - ez / 6)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 6, cy - ey / 6, cz - ez / 6)));

        foreach (Vector3 corner in corners)
        {
            GameObject hitObject = ServiceLocator.FindHitObject(corner);
            if (hitObject && hitObject.name != "Ground")
            {
                WorldObjects worldObject = hitObject.transform.GetComponent<WorldObjects>();
                if (worldObject && placeBounds.Intersects(worldObject.GetSelectionBounds()))
                    canPlace = false;
            }
        }

        corners.Clear();
        return canPlace;
    }

    private void Create()
    {
        int unitIndex = Random.Range(0, unitList.Count);

        if (unitList[unitIndex].GetComponent<VillagerController>())
        {
            float positionX = Random.Range(0f, 45f);
            float positionZ = Random.Range(0f, 45f);

            UnitController u = null;

            int position = Random.Range(0, 4);

            switch (position)
            {
                case 0:
                    u = Instantiate(unitList[unitIndex], new Vector3(townCenter.transform.position.x + positionX, 0.5f, townCenter.transform.position.z + positionZ), Quaternion.identity);
                    break;
                case 1:
                    u = Instantiate(unitList[unitIndex], new Vector3(townCenter.transform.position.x + positionX, 0.5f, townCenter.transform.position.z - positionZ), Quaternion.identity);
                    break;
                case 2:
                    u = Instantiate(unitList[unitIndex], new Vector3(townCenter.transform.position.x - positionX, 0.5f, townCenter.transform.position.z + positionZ), Quaternion.identity);
                    break;
                case 3:
                    u = Instantiate(unitList[unitIndex], new Vector3(townCenter.transform.position.x - positionX, 0.5f, townCenter.transform.position.z - positionZ), Quaternion.identity);
                    break;
            }
            
            u.SetLayer(myEnenmyLayer);
            u.GiveJob(townCenter);

            Build();
        }
        else
        {
            UnitController u = Instantiate(unitList[unitIndex], unitSpawnPoint[Random.Range(0, unitSpawnPoint.Length)].position, Quaternion.identity);
            u.SetLayer(myEnenmyLayer);

            if(u.GetComponent<VillagerController>() == null)
                AiUnits.AddUnitInList(u);

            Build();
        }
    }
}
