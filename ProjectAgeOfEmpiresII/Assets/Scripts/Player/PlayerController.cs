﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;

public class PlayerController : MonoBehaviour
{
    [SerializeField] public string username;
    [SerializeField] public bool human;

    [SerializeField] private HUDController _HUDReference;

    [Header("Resources")]

    public int startGold, startGoldLimit, startFood, startFoodLimit, startStone, startStoneLimit, startWood, startWoodLimit, startPopulation, startPopulationLimit;
    private Dictionary<ResourceType, int> resources, resourceLimits;

    private WorldObjects _selectedObject;

    [Header("Rally Point")]

    [SerializeField] private RallyPoint rallyPoint;

    public RallyPoint RallyPoint
    {
        get { return rallyPoint; }
    }

    //Building Section

    public Material notAllowedMaterial, allowedMaterial;

    private BuildingController tempBuilding;
    private UnitController tempCreator;
    public bool findingPlacement = false;

    //Properties Section

    public WorldObjects SelectedObject
    {
        get
        {
            return _selectedObject;
        }
        set
        {
            if (value != _selectedObject)
            {
                _selectedObject = value;

                if (_selectedObject != null)
                    _HUDReference.SetSelectedObject(_selectedObject.objectName);
                else
                    _HUDReference.ResetSelectionPanel();
            }
        }
    }

    public HUDController HUDReference
    {
        get { return _HUDReference; }
    }

    public bool Human
    {
        get { return human; }
    }

    void Awake()
    {
        resources = InitResourceList();
        resourceLimits = InitResourceList();
    }

    private void Start()
    {
        AddStartResourceLimits();
        AddStartResources();
    }

    private void Update()
    {
        if (human)
            HUDReference.SetResourceValues(resources, resourceLimits);

        if (findingPlacement)
        {
            tempBuilding.lineOfSightMesh.gameObject.SetActive(false);

            if (CanPlaceBuilding()) 
                tempBuilding.SetTransparentMaterial(allowedMaterial, false);
            else 
                tempBuilding.SetTransparentMaterial(notAllowedMaterial, false);
        }
    }

    //Resource Section

    private Dictionary<ResourceType, int> InitResourceList()
    {
        Dictionary<ResourceType, int> list = new Dictionary<ResourceType, int>();
        list.Add(ResourceType.Gold, 0);
        list.Add(ResourceType.Stone, 0);
        list.Add(ResourceType.Food, 0);
        list.Add(ResourceType.Wood, 0);
        list.Add(ResourceType.Population, 0);
        return list;
    }

    private void AddStartResourceLimits()
    {
        IncrementResourceLimit(ResourceType.Gold, startGoldLimit);
        IncrementResourceLimit(ResourceType.Stone, startStoneLimit);
        IncrementResourceLimit(ResourceType.Food, startFoodLimit);
        IncrementResourceLimit(ResourceType.Wood, startWoodLimit);
        IncrementResourceLimit(ResourceType.Population, startPopulationLimit);
    }

    private void AddStartResources()
    {
        AddResource(ResourceType.Gold, startGold);
        AddResource(ResourceType.Stone, startStone);
        AddResource(ResourceType.Food, startFood);
        AddResource(ResourceType.Wood, startWood);
        AddResource(ResourceType.Population, startPopulation);
    }

    public void AddResource(ResourceType type, int amount)
    {
        resources[type] += amount;
    }

    public void IncrementResourceLimit(ResourceType type, int amount)
    {
        resourceLimits[type] += amount;
    }

    public int GetPopulationLimits()
    {
        return resourceLimits[ResourceType.Population];
    }

    public int GetResource(ResourceType type)
    {
        int res = 0;
        switch (type)
        {
            case ResourceType.Food:
                res = resources[ResourceType.Food];
                break;
            case ResourceType.Wood:
                res = resources[ResourceType.Wood];
                break;
            case ResourceType.Gold:
                res = resources[ResourceType.Gold];
                break;
            case ResourceType.Stone:
                res = resources[ResourceType.Stone];
                break;
            case ResourceType.Population:
                res = resources[ResourceType.Population];
                break;
        }
        return res;
    }

    //Building Section

    public void AddUnit(string unitName, Vector3 spawnPoint, Vector3 rallyPoint, Quaternion rotation, BuildingController creator)
    {
        if(GetResource(ResourceType.Population) + ServiceLocator.GetUnit(unitName).GetComponent<UnitController>().populationCost <= GetPopulationLimits())
        {
            GameObject newUnit = (GameObject)Instantiate(ServiceLocator.GetUnit(unitName), spawnPoint, rotation);
            UnitController unitObject = newUnit.GetComponent<UnitController>();

            if (unitObject)
            {
                unitObject.SetBuilding(creator);
                if (spawnPoint != rallyPoint)
                    unitObject.SetStartDestination(rallyPoint, 0);
            }
        }

    }

    public void CreateBuilding(string buildingName, Vector3 buildPoint, UnitController creator)
    {
        GameObject newBuilding = (GameObject)Instantiate(ServiceLocator.GetBuilding(buildingName), buildPoint, new Quaternion());
        
        tempBuilding = newBuilding.GetComponent<BuildingController>();
        if (tempBuilding)
        {
            tempCreator = creator;
            findingPlacement = true;
            tempBuilding.SetTransparentMaterial(notAllowedMaterial, true);
            tempBuilding.SetColliders(false);
            
        }
        else 
            Destroy(newBuilding);
    }

    public bool IsFindingBuildingLocation()
    {
        return findingPlacement;
    }

    public void FindBuildingLocation()
    {
        Vector3 newLocation = ServiceLocator.FindHitPoint(Input.mousePosition);
        newLocation.y = tempBuilding.transform.localScale.y / 2 + 0.25f;
        tempBuilding.transform.position = newLocation;
    }

    public bool CanPlaceBuilding()
    {
        bool canPlace = true;

        Bounds placeBounds = tempBuilding.GetSelectionBounds();
        //shorthand for the coordinates of the center of the selection bounds
        float cx = placeBounds.center.x;
        float cy = placeBounds.center.y;
        float cz = placeBounds.center.z;
        //shorthand for the coordinates of the extents of the selection box
        float ex = placeBounds.extents.x;
        float ey = placeBounds.extents.y;
        float ez = placeBounds.extents.z;

        //Determine the screen coordinates for the corners of the selection bounds
        List<Vector3> corners = new List<Vector3>();

        corners.Add(Camera.main.WorldToScreenPoint(placeBounds.center));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex, cy + ey, cz + ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex, cy + ey, cz - ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex, cy - ey, cz + ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex, cy + ey, cz + ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex, cy - ey, cz - ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex, cy - ey, cz + ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex, cy + ey, cz - ez)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex, cy - ey, cz - ez)));

        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex/2, cy + ey/2, cz + ez/2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex/2, cy + ey/2, cz - ez/2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex/2, cy - ey/2, cz + ez/2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex/2, cy + ey/2, cz + ez/2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex/2, cy - ey/2, cz - ez/2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex/2, cy - ey/2, cz + ez/2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex/2, cy + ey/2, cz - ez/2)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex/2, cy - ey/2, cz - ez/2)));

        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 4, cy + ey / 4, cz + ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 4, cy + ey / 4, cz - ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 4, cy - ey / 4, cz + ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 4, cy + ey / 4, cz + ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex / 4, cy - ey / 4, cz - ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 4, cy - ey / 4, cz + ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 4, cy + ey / 4, cz - ez / 4)));
        corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex / 4, cy - ey / 4, cz - ez / 4)));

        foreach (Vector3 corner in corners)
        {
            GameObject hitObject = ServiceLocator.FindHitObject(corner);
            if (hitObject && hitObject.name != "Ground")
            {
                WorldObjects worldObject = hitObject.transform.GetComponent<WorldObjects>();
                if (worldObject && placeBounds.Intersects(worldObject.GetSelectionBounds())) 
                    canPlace = false;
            }
        }

        corners.Clear();
        return canPlace;
    }

    public void StartConstruction()
    {
        BuildingController buildings = GetComponentInChildren<BuildingController>();

        if (buildings) 
            tempBuilding.transform.parent = buildings.transform;

        tempBuilding.SetColliders(true);
        tempCreator.SetBuilding(tempBuilding);
        tempBuilding.StartConstruction();
        tempBuilding.lineOfSightMesh.gameObject.SetActive(true);

        findingPlacement = false;
    }

    public void CancelBuildingPlacement()
    {
        findingPlacement = false;
        Destroy(tempBuilding.gameObject);
        tempBuilding = null;
        tempCreator = null;
    }
}
