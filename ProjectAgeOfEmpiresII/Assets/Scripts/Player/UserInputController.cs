﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;

public class UserInputController : MonoBehaviour
{
    private PlayerController player;

    private bool isSelecting = false;
    private Vector3 mousePosition;
    private Vector3 lastMousePosition;
    Rect rect;

    //Double Click Section

    private int counter;
    private float doubleClickTimer = 0.25f;
    private int overlapSphereDistance = 70;

    //Minimap Section

    public Material minimapIndicatorMaterial;
    public MeshFilter filterminimap;
    private float _minimapIndicatorStrokeWidth = 0.1f; // relative to indicator size
    private Transform _minimapIndicator;
    private Mesh _minimapIndicatorMesh;

    // Start is called before the first frame update
    void Start()
    {
        player = transform.GetComponent<PlayerController>();

        PrepareMapIndicator();
    }

    private void OnGUI()
    {
        if (isSelecting)
        {
            if (player.HUDReference.MouseInBounds())
            {
                lastMousePosition = Input.mousePosition;
            }
            else
                lastMousePosition.x = Input.mousePosition.x;

            rect = Utils.GetScreenRect(mousePosition, lastMousePosition);
            Utils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            Utils.DrawScreenRectBorder(rect, 2, new Color(0.8f, 0.8f, 0.95f));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (player.Human)
        {
            if (Input.GetKeyDown(KeyCode.Space))
                CenterCameraOnUnit();

            MoveCamera();
            RotateCamera();
            MouseActivity();
        }
    }

    //Camera Movement Section

    private void MoveCamera()
    {
        float xpos = Input.mousePosition.x;
        float ypos = Input.mousePosition.y;

        Vector3 movement = new Vector3(0, 0, 0);
        bool mouseScroll = false;

        //Horizontal

        if (xpos >= 0 && xpos < ServiceLocator.ScrollWidth)
        {
            movement.x -= ServiceLocator.ScrollSpeed;
            player.HUDReference.SetCursorState(CursorState.PanLeft);
            mouseScroll = true;
        }         
        else if (xpos <= Screen.width && xpos > Screen.width - ServiceLocator.ScrollWidth)
        {
            movement.x += ServiceLocator.ScrollSpeed;
            player.HUDReference.SetCursorState(CursorState.PanRight);
            mouseScroll = true;
        }
            
        //Vertical

        if (ypos >= 0 && ypos < ServiceLocator.ScrollWidth)
        {
            movement.z -= ServiceLocator.ScrollSpeed;
            player.HUDReference.SetCursorState(CursorState.PanDown);
            mouseScroll = true;
        }
        else if (ypos <= Screen.height && ypos > Screen.height - ServiceLocator.ScrollWidth)
        {
            movement.z += ServiceLocator.ScrollSpeed;
            player.HUDReference.SetCursorState(CursorState.PanUp);
            mouseScroll = true;
        }

        if (!mouseScroll)
            player.HUDReference.SetCursorState(CursorState.Select);

        FixAltitude();
        ComputeMinimapIndicator(false);

        //Near

        movement = Camera.main.transform.TransformDirection(movement);
        movement.y = 0;

        //Far

        movement.y -= ServiceLocator.ScrollSpeed * Input.GetAxis("Mouse ScrollWheel");

        Vector3 origin = Camera.main.transform.position;
        Vector3 destination = origin;
        destination.x += movement.x;
        destination.y += movement.y;
        destination.z += movement.z;

        if (destination.y > ServiceLocator.MaxCameraHeight)
            destination.y = ServiceLocator.MaxCameraHeight;
        else if (destination.y < ServiceLocator.MinCameraHeight)
            destination.y = ServiceLocator.MinCameraHeight;

        if (destination != origin)
            Camera.main.transform.position = Vector3.MoveTowards(origin, destination, Time.deltaTime * ServiceLocator.ScrollSpeed);


    }

    private void FixAltitude()
    {
    }

    private void OnMoveCamera(Vector3 data)
    {
        Vector3 pos = data;

        float indicatorW = 70;
        float indicatorH = 70;

        pos.x -= indicatorW / 2f;
        pos.z -= indicatorH / 2f;

        Debug.Log(pos.x + " " + pos.z);

        Vector3 off = Camera.main.transform.position - Utils.MiddleOfScreenPointToWorld();

        Vector3 newPos = pos + off;

        newPos.y = 100f;
        Camera.main.transform.position = newPos;

        //FixAltitude();
        ComputeMinimapIndicator(false);
    }

    private void RotateCamera()
    {
        Vector3 origin = Camera.main.transform.eulerAngles;
        Vector3 destination = origin;

        //ALT + MousekeyDown (Right)

        if ((Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) && Input.GetMouseButton(1))
        {
            destination.x -= Input.GetAxis("Mouse Y") * ServiceLocator.RotateAmount;
            destination.y += Input.GetAxis("Mouse X") * ServiceLocator.RotateAmount;
        }

        if (destination != origin)
        {
            Camera.main.transform.eulerAngles = Vector3.MoveTowards(origin, destination, Time.deltaTime * ServiceLocator.RotateSpeed);
        }
    }

    //Mouse Activity Section

    private void MouseActivity()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(!Input.GetKey(KeyCode.LeftControl) && !player.findingPlacement)
                ClearSelection();

            LeftMouseClick();
        }
            

        if (Input.GetMouseButton(0))
            OnPressedLeftMouseClick();

        if (Input.GetMouseButtonUp(0))
        {
            LeftMouseRelease();
        }


        else if (Input.GetMouseButtonDown(1))
            RightMouseClick();

        MouseHover();
    }

    private Vector3 FindHitPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) return hit.point;
        return ServiceLocator.InvalidPosition;
    }

    private GameObject FindHitObject()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) 
            return hit.collider.gameObject;
        return null;
    }

    private void LeftMouseClick()
    {
        if (player.HUDReference.MouseInBounds())
        {
            if (player.IsFindingBuildingLocation())
            {
                if (player.CanPlaceBuilding())
                    player.StartConstruction();
            }
            else
            {
                if (player.HUDReference.MouseInBounds())
                {
                    GameObject hitObject = FindHitObject();
                    Vector3 hitPoint = FindHitPoint();

                    if (hitObject && hitPoint != ServiceLocator.InvalidPosition)
                    {
                        if (player.SelectedObject)
                            player.SelectedObject.MouseClick(hitObject, hitPoint, player);
                        else if (hitObject.name != "Ground")
                        {
                            WorldObjects worldObject = null;

                            if (!hitObject.GetComponent<ResourceController>())
                                worldObject = hitObject.transform.root.GetComponent<WorldObjects>();
                            else
                                worldObject = hitObject.transform.GetComponent<WorldObjects>();
                                

                            if (worldObject && (worldObject.gameObject.layer != 9 || worldObject.GetComponent<BuildingController>() != null))
                            {                            
                                player.SelectedObject = worldObject;
                                worldObject.SetSelection(true);

                                counter++;
                                if (counter == 1)
                                    StartCoroutine(DoubleClickDetector());
                                else
                                {
                                    if (counter == 2)
                                    {
                                        DoubleClickSelector();
                                        StopCoroutine(DoubleClickDetector());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (player.IsFindingBuildingLocation())
                player.CancelBuildingPlacement();
        }
    }

    IEnumerator DoubleClickDetector()
    {
        yield return new WaitForSeconds(doubleClickTimer);
        counter = 0;
    }

    private void DoubleClickSelector()
    {
        Collider[] objectInscene = Physics.OverlapSphere(player.SelectedObject.transform.position, overlapSphereDistance);

        for (int i = 0; i < objectInscene.Length; i++)
        {
            if (objectInscene[i].GetComponent<MeshRenderer>() && objectInscene[i].GetComponent<MeshRenderer>().isVisible)
            {
                if (objectInscene[i].GetComponent<WorldObjects>() && !objectInscene[i].GetComponent<ResourceController>())
                {
                    WorldObjects myObject = objectInscene[i].GetComponent<WorldObjects>();
                    if (objectInscene[i].GetComponent<WorldObjects>().objectName == player.SelectedObject.objectName)
                    {
                        if(objectInscene[i].GetComponent<WorldObjects>().gameObject.layer != 9)
                            objectInscene[i].GetComponent<WorldObjects>().SetSelection(true);
                    }

                }
            }
        }
    }

    private void OnPressedLeftMouseClick()
    {
        if (player.HUDReference.MouseInBounds() && !isSelecting)
        {
            GameObject hitObject = FindHitObject();
            mousePosition = Input.mousePosition;

            if (hitObject && mousePosition != ServiceLocator.InvalidPosition)
            {
                if (hitObject.name == "Ground")
                {
                    isSelecting = true;
                }
            }
        }

        if (isSelecting)
        {
            foreach (var selectableObject in FindObjectsOfType<UnitController>())
            {
                if (IsWithinSelectionBounds(selectableObject.gameObject))
                {
                    WorldObjects worldObj = selectableObject.gameObject.GetComponent<WorldObjects>();

                    if(worldObj.gameObject.layer != 9)
                    {
                        worldObj.SetSelection(true);
                        player.SelectedObject = worldObj;
                    }
                }
            }
        }
    }

    public bool IsWithinSelectionBounds(GameObject gameObject)
    {
        if (lastMousePosition != Vector3.zero)
        {
            Bounds viewportBounds = Utils.GetViewportBounds(Camera.main, mousePosition, lastMousePosition);
            return viewportBounds.Contains(Camera.main.WorldToViewportPoint(gameObject.transform.position));
        }

        return false;
    }

    private void LeftMouseRelease()
    {
        isSelecting = false;
    }

    private void RightMouseClick()
    {
        if (player.HUDReference.MouseInBounds())
        {
            GameObject hitObject = FindHitObject();
            Vector3 hitPoint = FindHitPoint();
            if (hitObject && hitPoint != ServiceLocator.InvalidPosition)
            {
                if (player.IsFindingBuildingLocation())
                {
                    player.CancelBuildingPlacement();
                }
                else
                {
                    if (player.SelectedObject)
                    {
                        if (player.SelectedObject.GetComponent<UnitController>())
                        {
                            for (int i = 0; i < ServiceLocator.selectionList.Count; i++)
                            {
                                ServiceLocator.selectionList[i].MouseClick(hitObject, hitPoint, player);
                            }
                        }
                        else
                            player.SelectedObject.MouseClick(hitObject, hitPoint, player);
                    }
                }
            }
        }
    }

    private void CenterCameraOnUnit()
    {
        if (player.SelectedObject)
        {
            Ray r = Camera.main.ViewportPointToRay(new Vector3(.5f, .5f, 0));
            RaycastHit hit;

            if (Physics.Raycast(r, out hit))
            {
                Vector3 click = hit.point;
                Vector3 dir = player.SelectedObject.transform.position - click;
                Camera.main.transform.position += dir;

            }
        }
    }

    private void ClearSelection()
    {
        if (player.HUDReference.MouseInBounds() && !Input.GetKey(KeyCode.LeftAlt) && player.SelectedObject)
        {
            lastMousePosition = Vector3.zero;
            player.SelectedObject.SetSelection(false);
            player.SelectedObject = null;
        }  
    }

    private void MouseHover()
    {
        if (player.HUDReference.MouseInBounds())
        {
            if (player.IsFindingBuildingLocation())
            {
                player.FindBuildingLocation();
            }
            else
            {
                GameObject hoverObject = FindHitObject();

                if (hoverObject)
                {
                    if (player.SelectedObject)
                        player.SelectedObject.SetHoverState(hoverObject);
                    else if (hoverObject.name != "Ground")
                    {
                        WorldObjects owner = hoverObject.transform.root.GetComponent<WorldObjects>();

                        if (owner)
                        {
                            UnitController unit = hoverObject.transform.root.GetComponent<UnitController>();
                            BuildingController building = hoverObject.transform.root.GetComponent<BuildingController>();



                            if (unit || building)
                                player.HUDReference.SetCursorState(CursorState.Select);
                        }
                    }
                }
            }
        }
    }

    private void PrepareMapIndicator()
    {
        GameObject g = new GameObject("MinimapIndicator");
        _minimapIndicator = g.transform;
        g.layer = 14; // put on "Minimap" layer
        _minimapIndicator.position = Vector3.zero;
        MeshFilter mf = g.AddComponent<MeshFilter>();
        mf.mesh = filterminimap.mesh;
        MeshRenderer mr = g.AddComponent<MeshRenderer>();
        mr.material = new Material(minimapIndicatorMaterial);
        ComputeMinimapIndicator(true);
    }


    private void ComputeMinimapIndicator(bool zooming)
    {
        Vector3 middle = Utils.MiddleOfScreenPointToWorld();

        _minimapIndicator.position = new Vector3(middle.x, 1, middle.z);
        _minimapIndicator.localScale = new Vector3(70, 1, 70);
        _minimapIndicator.localEulerAngles = new Vector3(0, 0, 0);
    }
}
