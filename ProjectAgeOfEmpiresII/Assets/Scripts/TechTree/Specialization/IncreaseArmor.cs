﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseArmor : TechNode
{
    public int amount = 5;

    public List<UnitController> unitsToUpgrade = new List<UnitController>();

    public override void ApplyEffect()
    {
        for (int i = 0; i < unitsToUpgrade.Count; i++)
        {
            if(unitsToUpgrade[i].gameObject.layer != 9)
                unitsToUpgrade[i].armorValue += amount;
        }

        UnitController[] units = FindObjectsOfType<UnitController>();

        for (int i = 0; i < units.Length; i++)
        {
            for (int j = 0; j < unitsToUpgrade.Count; j++)
            {
                if (units[i].name == unitsToUpgrade[j].name)
                {
                    if (units[i].gameObject.layer != 9)
                    {
                        if (units[i].name == unitsToUpgrade[j].name)
                            units[i].armorValue += amount;
                    }
                }
            }
        }
    }
}
