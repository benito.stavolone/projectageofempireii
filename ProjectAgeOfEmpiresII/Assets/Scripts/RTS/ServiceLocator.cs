﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS
{
    public static class ServiceLocator
    {
        //Camera Settings

        public static float ScrollSpeed { get { return 25; } }
        public static float RotateSpeed { get { return 100; } }
        public static float RotateAmount { get { return 10; } }
        public static int ScrollWidth { get { return 30; } }
        public static float MinCameraHeight { get { return 20; } }
        public static float MaxCameraHeight { get { return 50; } }

        private static Vector3 invalidPosition = new Vector3(-99999, -99999, -99999);
        public static Vector3 InvalidPosition { get { return invalidPosition; } }

        //Selection Settings

        public static List<UnitController> selectionList = new List<UnitController>();

        public static Vector3 FindHitPoint(Vector3 origin)
        {
            Ray ray = Camera.main.ScreenPointToRay(origin);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) return hit.point;
            return InvalidPosition;
        }

        public static GameObject FindHitObject(Vector3 origin)
        {
            Ray ray = Camera.main.ScreenPointToRay(origin);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) return hit.collider.gameObject;
            return null;
        }

        //Building Settings

        public static int BuildSpeed { get { return 2; } }

        private static GameObjectList gameObjectList;

        public static void SetGameObjectList(GameObjectList objectList)
        {
            gameObjectList = objectList;
        }

        public static GameObject GetBuilding(string name)
        {
            return gameObjectList.GetBuilding(name);
        }

        public static GameObject GetUnit(string name)
        {
            return gameObjectList.GetUnit(name);
        }

        public static GameObject GetWorldObject(string name)
        {
            return gameObjectList.GetWorldObject(name);
        }

        public static GameObject GetUpgrade(string name)
        {
            return gameObjectList.GetUpgrade(name);
        }

        public static GameObject GetPlayerObject()
        {
            return gameObjectList.GetPlayerObject();
        }

        public static Sprite GetBuildImage(string name)
        {
            return gameObjectList.GetBuildImage(name);
        }

        public static int GetFoodCost(string name)
        {
            return gameObjectList.GetFoodCost(name);
        }

        public static int GetWoodCost(string name)
        {
            return gameObjectList.GetWoodCost(name);
        }

        public static int GetGoldCost(string name)
        {
            return gameObjectList.GetGoldCost(name);
        }

        public static int GetStoneCost(string name)
        {
            return gameObjectList.GetStoneCost(name);
        }

        public static int GetPopulationCost(string name)
        {
            return gameObjectList.GetPopulationCost(name);
        }



        public static List<WorldObjects> FindNearbyObjects(Vector3 position, float range)
        {
            Collider[] hitColliders = Physics.OverlapSphere(position, range);
            List<WorldObjects> nearbyObjects = new List<WorldObjects>();
            for (int i = 0; i < hitColliders.Length; i++)
            {
                Transform parent = hitColliders[i].transform.parent;

                if (parent)
                {
                    WorldObjects parentObject = parent.GetComponent<WorldObjects>();
                    nearbyObjects.Add(parentObject);
                }
            }

            return nearbyObjects;
        }

    }
}

