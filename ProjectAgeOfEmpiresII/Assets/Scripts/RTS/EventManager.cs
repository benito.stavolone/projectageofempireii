﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;

public class EventManager : MonoBehaviour
{
    public static Action<ResourceType, BuildingController> storageBuilt;

    public static Action armoryBuilt;

    public static Action newAge;

    public static Action<bool> dinDon;

    public static Action<string> gameOver;

    public static void StorageBuilt(ResourceType resource, BuildingController storagePosition)
    {
        storageBuilt?.Invoke(resource, storagePosition);
    }

    public static void ArmoryBuilt()
    {
        armoryBuilt?.Invoke();
    }

    public static void NewAge()
    {
        newAge?.Invoke();
    }

    public static void DinDon(bool inside)
    {
        dinDon?.Invoke(inside);
    }

    public static void GameOver(string text)
    {
        gameOver.Invoke(text);
    }
}
