﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS
{
    public enum CursorState { Select, Move, Attack, PanLeft, PanRight, PanUp, PanDown, Resources }

    public enum ResourceType { Gold, Stone, Food, Wood, Population, Unknown }
}
