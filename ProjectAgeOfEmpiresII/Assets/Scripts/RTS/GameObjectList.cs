﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;

public class GameObjectList : MonoBehaviour
{
    public GameObject[] buildings;
    public GameObject[] units;
    public GameObject[] worldObjects;
    public GameObject[] upgrades;
    public GameObject player;

    private static bool created = false;

    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(transform.gameObject);
            ServiceLocator.SetGameObjectList(this);
            created = true;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public GameObject GetBuilding(string name)
    {
        for (int i = 0; i < buildings.Length; i++)
        {
            BuildingController building = buildings[i].GetComponent<BuildingController>();
            if (building && building.name == name) return buildings[i];
        }
        return null;
    }

    public GameObject GetUnit(string name)
    {
        for (int i = 0; i < units.Length; i++)
        {
            UnitController unit = units[i].GetComponent<UnitController>();
            if (unit && unit.name == name) return units[i];
        }
        return null;
    }

    public GameObject GetWorldObject(string name)
    {
        foreach (GameObject worldObject in worldObjects)
        {
            if (worldObject.name == name) return worldObject;
        }
        return null;
    }

    public GameObject GetUpgrade(string name)
    {
        foreach (GameObject upgrade in upgrades)
        {
            if (upgrade.name == name) 
                return upgrade;
        }
        return null;
    }

    public GameObject GetPlayerObject()
    {
        return player;
    }

    public Sprite GetBuildImage(string name)
    {
        for (int i = 0; i < buildings.Length; i++)
        {
            BuildingController building = buildings[i].GetComponent<BuildingController>();
            if (building && building.name == name) 
                return building.buildImage;
        }
        for (int i = 0; i < units.Length; i++)
        {
            UnitController unit = units[i].GetComponent<UnitController>();
            if (unit && unit.name == name) 
                return unit.buildImage;
        }
        for (int i = 0; i < worldObjects.Length; i++)
        {
            ResourceController resource = worldObjects[i].GetComponent<ResourceController>();
            if (resource && resource.name == name)
                return resource.buildImage;
        }

        for (int i = 0; i < upgrades.Length; i++)
        {
            TechNode upgrade = upgrades[i].GetComponent<TechNode>();
            if (upgrade && upgrade.name == name)
                return upgrade.buildImage;
        }
        return null;
    }

    public int GetFoodCost(string name)
    {
        for (int i = 0; i < buildings.Length; i++)
        {
            BuildingController building = buildings[i].GetComponent<BuildingController>();
            if (building && building.name == name)
                return building.foodCost;
        }
        for (int i = 0; i < units.Length; i++)
        {
            UnitController unit = units[i].GetComponent<UnitController>();
            if (unit && unit.name == name)
                return unit.foodCost;
        }
        for (int i = 0; i < upgrades.Length; i++)
        {
            TechNode upgrade = upgrades[i].GetComponent<TechNode>();
            if (upgrade && upgrade.name == name)
                return upgrade.foodCost;
        }
        return 0;
    }

    public int GetWoodCost(string name)
    {
        for (int i = 0; i < buildings.Length; i++)
        {
            BuildingController building = buildings[i].GetComponent<BuildingController>();
            if (building && building.name == name)
                return building.woodCost;
        }
        for (int i = 0; i < units.Length; i++)
        {
            UnitController unit = units[i].GetComponent<UnitController>();
            if (unit && unit.name == name)
                return unit.woodCost;
        }
        for (int i = 0; i < upgrades.Length; i++)
        {
            TechNode upgrade = upgrades[i].GetComponent<TechNode>();
            if (upgrade && upgrade.name == name)
                return upgrade.woodCost;
        }
        return 0;
    }

    public int GetGoldCost(string name)
    {
        for (int i = 0; i < buildings.Length; i++)
        {
            BuildingController building = buildings[i].GetComponent<BuildingController>();
            if (building && building.name == name)
                return building.goldCost;
        }
        for (int i = 0; i < units.Length; i++)
        {
            UnitController unit = units[i].GetComponent<UnitController>();
            if (unit && unit.name == name)
                return unit.goldCost;
        }
        for (int i = 0; i < upgrades.Length; i++)
        {
            TechNode upgrade = upgrades[i].GetComponent<TechNode>();
            if (upgrade && upgrade.name == name)
                return upgrade.goldCost;
        }
        return 0;
    }

    public int GetStoneCost(string name)
    {
        for (int i = 0; i < buildings.Length; i++)
        {
            BuildingController building = buildings[i].GetComponent<BuildingController>();
            if (building && building.name == name)
                return building.stoneCost;
        }
        for (int i = 0; i < units.Length; i++)
        {
            UnitController unit = units[i].GetComponent<UnitController>();
            if (unit && unit.name == name)
                return unit.stoneCost;
        }
        for (int i = 0; i < upgrades.Length; i++)
        {
            TechNode upgrade = upgrades[i].GetComponent<TechNode>();
            if (upgrade && upgrade.name == name)
                return upgrade.stoneCost;
        }
        return 0;
    }

    public int GetPopulationCost(string name)
    {
        for (int i = 0; i < units.Length; i++)
        {
            UnitController unit = units[i].GetComponent<UnitController>();
            if (unit && unit.name == name)
                return unit.populationCost;
        }
        return 0;
    }
}
