﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.SceneManagement;

public class HUDController : MonoBehaviour
{
    public PlayerController player;

    [Header("Selection Panel")]

    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private Image buildImage;

    public TextMeshProUGUI atkText;
    public TextMeshProUGUI defText;

    [SerializeField] private GameObject buttonSelection;
    [SerializeField] private GameObject container1;
    [SerializeField] private GameObject container2;
    public Slider healthSlider;

    private List<GameObject> buttonInstantiate = new List<GameObject>();

    [Header("ToolTip Panel")]

    [SerializeField] private GameObject tooltipPanel;
    [SerializeField] private TextMeshProUGUI costText;

    [Header("Resources Panel")]

    [SerializeField] private TextMeshProUGUI goldText, foodText, stoneText, woodText, populationText, populationLimitText;

    private Dictionary<ResourceType, int> resourceValues, resourceLimits;

    [Header("Action Object")]

    [SerializeField] private GameObject panelAction;


    [HideInInspector]
    public Button[] buttonReference;
    private List<string> myActions;

    [Header("Creation Object")]

    [SerializeField] private GameObject panelCreation;
    private BuildingController selectedBuilding;

    [Header("Ages UI")]

    [SerializeField] private Slider agesSlider;
    [SerializeField] private GameObject feudalAgeImage, thirdAge, quarterAge;

    [Header("Minimap")]

    public Camera MinimapCamera;
    public GameObject Minimap;
    public GameObject point;
    public GameObject mainCamerapoint;
    public float MapWidth;
    public float MapHeight;

    public GameObject PanelCreation
    {
        get { return panelCreation; }
    }

    [Header("Mouse Textures")]

    [SerializeField] private GUISkin mouseCursorSkin;
    [SerializeField] private Texture2D activeCursor;
    [SerializeField] private Texture2D selectCursor, leftCursor, rightCursor, upCursor, downCursor;
    [SerializeField] private Texture2D[] moveCursors, attackCursors, resourcesCursors;

    private CursorState activeCursorState;
    private int currentFrame = 0;

    [Header("GameOver")]

    public GameObject gameOverPanel;
    public TextMeshProUGUI gameOverText;

    private void Start()
    {
        resourceValues = new Dictionary<ResourceType, int>();
        resourceLimits = new Dictionary<ResourceType, int>();

        healthSlider.gameObject.SetActive(false);

        SetCursorState(CursorState.Select);
    }

    private void OnEnable()
    {
        EventManager.newAge += UpdateAgesUI;

        EventManager.gameOver += OnGameOver;
    }

    private void OnDisable()
    {
        EventManager.newAge -= UpdateAgesUI;

        EventManager.gameOver -= OnGameOver;
    }

    private void OnGUI()
    {
        DrawMouseCursor();
    }

    private void Update()
    {
        CheckEndSelection();

        if (Input.GetKeyDown(KeyCode.R))
        {

            ServiceLocator.selectionList.Clear();

            SceneManager.LoadScene(0);

        }
    }

    //SelectionBox Section

    public bool MouseInBounds()
    {
        Vector3 mousePos = Input.mousePosition;
        bool insideHeight = mousePos.y >= 200 && mousePos.y <= Screen.height - 50;
        return insideHeight;
    }

    public void SetSelectedObject(string selectionName)
    {
        if (player.SelectedObject != null)
        {
            if (!selectionName.Equals(""))
            {
                if (ServiceLocator.selectionList.Count <= 1)
                {
                    container1.SetActive(true);
                    container2.SetActive(false);

                    nameText.text = selectionName;
                    buildImage.sprite = ServiceLocator.GetBuildImage(selectionName);

                    if(player.SelectedObject.gameObject.layer != 9)
                        ActiveActionPanel();
                }
            }
        }
    }

    public void ActiveActionPanel()
    {
        if (player.SelectedObject == null)
            return;

        if (player.SelectedObject.GetComponent<BuildingController>())
        {
            if (!player.SelectedObject.GetComponent<BuildingController>().UnderConstruction())
            {
                panelAction.SetActive(true);
                myActions = player.SelectedObject.GetActions();
                SetActionButton(myActions);
            }
        }
        else
        {
            panelAction.SetActive(true);
            myActions = player.SelectedObject.GetActions();
            SetActionButton(myActions);
        }
    }

    private void CheckEndSelection()
    {
        if (ServiceLocator.selectionList.Count > 1)
        {
            if (Input.GetMouseButtonUp(0) && buttonInstantiate.Count==0)
            {
                container1.SetActive(false);
                container2.SetActive(true);

                for (int i = 0; i < ServiceLocator.selectionList.Count; i++)
                {
                    GameObject myButton = Instantiate(buttonSelection, container2.transform);
                    buttonInstantiate.Add(myButton);
                    myButton.GetComponent<Button>().image.sprite = ServiceLocator.GetBuildImage(ServiceLocator.selectionList[i].objectName);
                    myButton.GetComponentInChildren<Slider>().value = ServiceLocator.selectionList[i].GetCurrentHealth();
                }
            }
        }
    }

    public void ResetSelectionPanel()
    {
        container1.SetActive(false);
        container2.SetActive(false);

        nameText.text = "";
        panelAction.SetActive(false);

        for (int i = 0; i < buttonReference.Length; i++)
            buttonReference[i].gameObject.SetActive(false);

        for (int i = 0; i < buttonInstantiate.Count; i++)
            Destroy(buttonInstantiate[i]);

        buttonInstantiate.Clear();         
    }

    //Mouse Cursor Section

    private void DrawMouseCursor()
    {
        bool mouseOverHud = !MouseInBounds() && activeCursorState != CursorState.PanRight && activeCursorState != CursorState.PanUp;

        if (mouseOverHud)
            Cursor.visible = true;
        else
        {
            Cursor.visible = false;
            GUI.skin = mouseCursorSkin;
            GUI.BeginGroup(new Rect(0, 0, Screen.width, Screen.height));
            UpdateCursorAnimation();
            Rect cursorPosition = GetCursorDrawPosition();
            GUI.Label(cursorPosition, activeCursor);
            GUI.EndGroup();
        }
    }

    private void UpdateCursorAnimation()
    {
        if (activeCursorState == CursorState.Move)
        {
            currentFrame = (int)Time.time % moveCursors.Length;
            activeCursor = moveCursors[currentFrame];
        }
        else if (activeCursorState == CursorState.Attack)
        {
            currentFrame = (int)Time.time % attackCursors.Length;
            activeCursor = attackCursors[currentFrame];
        }
        else if (activeCursorState == CursorState.Resources)
        {
            currentFrame = (int)Time.time % resourcesCursors.Length;
            activeCursor = resourcesCursors[currentFrame];
        }
    }

    private Rect GetCursorDrawPosition()
    {
        //set base position for custom cursor image
        float leftPos = Input.mousePosition.x;
        float topPos = Screen.height - Input.mousePosition.y; //screen draw coordinates are inverted
                                                              //adjust position base on the type of cursor being shown
        if (activeCursorState == CursorState.PanRight)
            leftPos = Screen.width - activeCursor.width;
        else if (activeCursorState == CursorState.PanDown)
            topPos = Screen.height - activeCursor.height;
        else if (activeCursorState == CursorState.Move || activeCursorState == CursorState.Select || activeCursorState == CursorState.Resources)
        {
            topPos -= activeCursor.height / 2;
            leftPos -= activeCursor.width / 2;
        }

        return new Rect(leftPos, topPos, activeCursor.width * 2, activeCursor.height * 2);
    }

    public void SetCursorState(CursorState newState)
    {
        activeCursorState = newState;
        switch (newState)
        {
            case CursorState.Select:
                activeCursor = selectCursor;
                break;
            case CursorState.Attack:
                currentFrame = (int)Time.time % attackCursors.Length;
                activeCursor = attackCursors[currentFrame];
                break;
            case CursorState.Resources:
                currentFrame = (int)Time.time % resourcesCursors.Length;
                activeCursor = resourcesCursors[currentFrame];
                break;
            case CursorState.Move:
                currentFrame = (int)Time.time % moveCursors.Length;
                activeCursor = moveCursors[currentFrame];
                break;
            case CursorState.PanLeft:
                activeCursor = leftCursor;
                break;
            case CursorState.PanRight:
                activeCursor = rightCursor;
                break;
            case CursorState.PanUp:
                activeCursor = upCursor;
                break;
            case CursorState.PanDown:
                activeCursor = downCursor;
                break;
            default: break;
        }
    }

    //Resources Bar Section

    public void SetResourceValues(Dictionary<ResourceType, int> resourceValues, Dictionary<ResourceType, int> resourceLimits)
    {
        this.resourceValues = resourceValues;
        this.resourceLimits = resourceLimits;

        SetResourcesValuesIntext();
    }

    private void SetResourcesValuesIntext()
    {
        foodText.text = resourceValues[ResourceType.Food].ToString();
        woodText.text = resourceValues[ResourceType.Wood].ToString();
        stoneText.text = resourceValues[ResourceType.Stone].ToString();
        goldText.text = resourceValues[ResourceType.Gold].ToString();

        populationText.text = resourceValues[ResourceType.Population].ToString();
        populationLimitText.text = "/ " + resourceLimits[ResourceType.Population].ToString();
    }

    //Actions Section

    private void SetActionButton(List<string> actions)
    {
        for (int i = 0; i < actions.Count; i++)
        {
            buttonReference[i].gameObject.SetActive(true);
            buttonReference[i].image.sprite = ServiceLocator.GetBuildImage(actions[i]);
        }
    }

    public void PerformAction(int index)
    {
        if (player.SelectedObject)
            player.SelectedObject.PerformAction(myActions[index], index);
    }

    //ToolTip Section

    public void ShowToolTip(int index)
    {
        try
        {
            tooltipPanel.SetActive(true);

            List<string> myActions = player.SelectedObject.GetActions();

            if (myActions[index] != null)
                costText.text = myActions[index];
            else
                costText.text = "";

            if (ServiceLocator.GetFoodCost(player.SelectedObject.GetActions()[index]) > 0)
            {
                costText.text = costText.text + "\n" + "Food: " + ServiceLocator.GetFoodCost(player.SelectedObject.GetActions()[index]).ToString();
            }

            if (ServiceLocator.GetWoodCost(player.SelectedObject.GetActions()[index]) > 0)
            {
                costText.text = costText.text + "\n" + "Wood: " + ServiceLocator.GetWoodCost(player.SelectedObject.GetActions()[index]).ToString();
            }

            if (ServiceLocator.GetGoldCost(player.SelectedObject.GetActions()[index]) > 0)
            {
                costText.text = costText.text + "\n" + "Gold: " + ServiceLocator.GetGoldCost(player.SelectedObject.GetActions()[index]).ToString();
            }

            if (ServiceLocator.GetStoneCost(player.SelectedObject.GetActions()[index]) > 0)
            {
                costText.text = costText.text + "\n" + "Stone: " + ServiceLocator.GetStoneCost(player.SelectedObject.GetActions()[index]).ToString();
            }

            if (ServiceLocator.GetPopulationCost(player.SelectedObject.GetActions()[index]) > 0)
            {
                costText.text = costText.text + "\n" + "Population: " + ServiceLocator.GetPopulationCost(player.SelectedObject.GetActions()[index]).ToString();
            }
        }
        catch(Exception e)
        {

        }

        
    }

    public void HideToolTip()
    {
        tooltipPanel.SetActive(false);
        costText.text = "";
    }

    //Ages Section

    private void UpdateAgesUI()
    {
        agesSlider.gameObject.SetActive(true);

        StartCoroutine(UpdateSlider());
    }

    private IEnumerator UpdateSlider()
    {
        float currentProgress = 0;

        while(agesSlider.value < 1)
        {
            currentProgress += Time.deltaTime * ServiceLocator.BuildSpeed;
            agesSlider.value = 0 + (currentProgress / 10);
            yield return null;
        }

        agesSlider.gameObject.SetActive(false);
        ActiveFeudalImage();
    }

    public void ActiveFeudalImage()
    {
        feudalAgeImage.SetActive(true);
    }

    //Minimap Section

    Rect screenRect;

    public void MinimapClick()
    {
        var miniMapRect = Minimap.GetComponent<RectTransform>().rect;

        screenRect = new Rect(
            Minimap.transform.position.x,
            Minimap.transform.position.y,
            miniMapRect.width, miniMapRect.height);

        var mousePos = Input.mousePosition;
        mousePos.y -= screenRect.y;
        mousePos.x -= screenRect.x;

        var camPos = new Vector3(
            (mousePos.x / 2) * (MapWidth / screenRect.width),
            Camera.main.transform.position.y,
            (mousePos.y / 2) * (MapHeight / screenRect.height));

        point.transform.position = camPos;

        Camera.main.transform.position = mainCamerapoint.transform.position;
    }

    private void OnGameOver(string text)
    {
        Time.timeScale = 0;
        gameOverPanel.SetActive(true);
        gameOverText.text = text;
    }

    public void OnClickRestart()
    {
        Time.timeScale = 1;

        ServiceLocator.selectionList.Clear();

        SceneManager.LoadScene(0);
    }

    public void OnClickQuit()
    {
        Application.Quit();
    }
}

